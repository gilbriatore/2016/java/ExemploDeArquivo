import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Programa {

	public static void main(String[] args) throws IOException {

		//exemploDeString();
		//exemploDeGravacao();
		//exemploTabuada();
		
		//Cria��o de uma pasta;
		File pasta = new File("teste");
		pasta.mkdir();
		
		//Cria��o de um arquivo;
		File arquivo = new File("teste/arquivo.txt");
		FileWriter fw = new FileWriter(arquivo);
		BufferedWriter bw = new BufferedWriter(fw);
				
		bw.write("Algum texto");
		
		bw.close();
		fw.close();
		
		//Leitura de arquivo
		Scanner leitor = new Scanner(arquivo);
		String str = leitor.nextLine();
		System.out.println(str);
		
		
		File arquivoNumero = new File("numeros.txt");
		Scanner sc = new Scanner(arquivoNumero);
		
//		String linha1 = sc.nextLine();
//		System.out.println(linha1);
//		String linha2 = sc.nextLine();
//		System.out.println(linha2);
		
		while (sc.hasNext()){
			String linha = sc.nextLine();
			System.out.println(linha); 
		}
		
		
	}

	private static void exemploTabuada() throws IOException {
		File arquivo = new File("tabuada.txt");
		FileWriter fw = new FileWriter(arquivo);
				
		for (int i = 1; i <= 10; i++) {
			//System.out.println("Tabuada do " + i); 
			for (int j = 1; j <= 10; j++) {
				//System.out.println(i + " x " + j + " = " + (i * j));	
				fw.write(i + " x " + j + " = " + (i * j) + "\n");
			}	
			fw.write("\n");
		}
		fw.close();
	}




	private static void exemploDeGravacao() throws IOException {
		String nota = "Paulo;Matem�tica;10";
		
		//Grava a nota no arquivo;
		File arquivo = new File("notas.txt");
		FileWriter fw = new FileWriter(arquivo);
		fw.write(nota); 
		fw.close();
		
		//Arquivo com sequ�ncia num�rica;
		File arqSeq = new File("numeros.txt");
		FileWriter fwseq = new FileWriter(arqSeq);
		
		for (int i = 1; i <= 1000; i++) {
			fwseq.write("N�mero " + i + "\n");
			
		}
		fwseq.close();
		
		System.out.println("Arquivo gravado com sucesso!");
	}
	
	
	

	private static void exemploDeString() {
		String str = "Ol� mundo!";
		System.out.println(str);
		
		//Tamanho da string;
		int tamanho = str.length();
		System.out.println(tamanho);
		
		//Caractere no �ndice 2;
		char caractere = str.charAt(2);
		System.out.println(caractere);
		
		//Cria��o de um substring;
		String substr = str.substring(4, str.length());
		System.out.println(substr);
		
		//Texto delimitado por ponto-e-v�rgula;
		String texto = "Este � o in�cio;Meio;Fim";
		System.out.println(texto);
		String[] textos = texto.split(";");
		System.out.println(textos[0]);
		System.out.println(textos[1]);
		System.out.println(textos[2]);
		
		
		//if (texto.contains("meio") || texto.contains("Meio")){
		if(texto.toLowerCase().contains("meio")){
			System.out.println("Verdadeiro!"); 
		} else {
			System.out.println("Falso!");
		}
		
		//Compara��o de String ou Objetos;
		String str1 = new String("abc");
		String str2 = new String("abc");
		
		//if (str1 == str2){ //Somente para tipos primitivos;
		if (str1.equals(str2)) { //Para compara��o de objetos
			System.out.println("Strings iguais!");
		} else {
			System.out.println("Strings diferentes!");
		}
	}
}